from conans import ConanFile, CMake
from conans.tools import get

class core(ConanFile):
	name="core"
	version="1.1.5"
	description="""
		The Core Library provides basic types and utilities that can be used between different devices. 
		This allows different libraries to share common items between themselves to avoid reimplementation and promote consistency.
		"""
	url="https://github.com/cypresssemiconductorco/core-lib"
	license="Apache License Version 2.0"
	author="Content: Cypress Semiconductor Corporation ; Package: Mathias Spiessens"
	build_policy="missing"
	settings = { "arch": ["armv6", "armv7", "armv7hf"], "os": ["none"], "build_type": ["Release", "Debug"], "compiler": ["gcc"] }

	def source(self):
		get("https://github.com/cypresssemiconductorco/core-lib/archive/release-v1.1.5.tar.gz")

	def package(self):
		self.copy("*.h", "include/", "core-lib-release-v1.1.5/include/")

	def package_info(self):
		self.cpp_info.includedirs = ["include/"]
