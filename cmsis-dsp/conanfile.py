from conans import ConanFile, CMake
from conans.tools import get

class cmsis_dsp(ConanFile):
	name="cmsis-dsp"
	version="5.7.0"
	description="""
		A suite of common signal processing functions for use on Cortex-M and Cortex-A processor based devices.
		"""
	url="https://github.com/ARM-software/CMSIS_5"
	license="Apache License Version 2.0"
	author="Content: ARM ; Package: Mathias Spiessens"
	build_policy="missing"
	requires="cmsis-core/5.7.0@arm/stable"
	settings = { "arch": ["armv6", "armv7", "armv7hf"], "os": ["none"], "build_type": ["Release", "Debug"], "compiler": ["gcc"] }
	generators = "cmake"
	exports_sources = "CMakeLists.txt"

	def source(self):
		get("https://github.com/ARM-software/CMSIS_5/archive/5.7.0.tar.gz")

	def build(self):
		cmake = CMake(self)

		cmake.definitions["CMAKE_TRY_COMPILE_TARGET_TYPE"] = "STATIC_LIBRARY"
		cmake.definitions["EXTRACTED_SOURCES"] = "CMSIS_5-5.7.0/"
		if self.settings.arch == "armv6":
			cmake.definitions["CMAKE_C_FLAGS_INIT"] = "-march=armv6-m -mthumb -ffunction-sections"
		elif self.settings.arch == "armv7":
			cmake.definitions["CMAKE_C_FLAGS_INIT"] = "-march=armv7e-m -mthumb -ffunction-sections"
		elif self.settings.arch == "armv7hf":
			cmake.definitions["CMAKE_C_FLAGS_INIT"] = "-march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -ffunction-sections"

		cmake.configure()

		cmake.build()

	def package(self):
		self.copy("*.h", "include/", "CMSIS_5-5.7.0/CMSIS/DSP/Include/")
		if self.settings.build_type == "Debug":
			self.copy("*.c", "source/", "CMSIS_5-5.7.0/CMSIS/DSP/Source/")
		self.copy("*.a", "library/", "lib/")

	def package_info(self):
		self.cpp_info.includedirs = ["include/"]
		self.cpp_info.srcdirs = ["source/"]
		self.cpp_info.libdirs = ["library/"]
		self.cpp_info.libs = ["dsp"]
