#include "arm_math.h"

int main(int argc, char *argv[])
{
    float in[] = {1.0f, 2.0f, 3.0f};
    float out;
    uint32_t where = 3;

    arm_max_f32(in, 3, &out, &where);

    return (int)out;
}
