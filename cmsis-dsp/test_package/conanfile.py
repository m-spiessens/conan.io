from conans import ConanFile, CMake

class TEST_dsp(ConanFile):
      settings = { "arch": ["armv7hf"], "os": ["none"], "build_type": ["Release", "Debug"], "compiler": ["gcc"] }
      generators = "cmake"

      def test(self):
            cmake = CMake(self)
            cmake.definitions["CONAN_C_FLAGS"] = "-march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16"
            cmake.definitions["CONAN_CXX_FLAGS"] = "-march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16"
            cmake.configure()
            cmake.build()
