from conans import ConanFile, CMake

class TEST_mbedTLS(ConanFile):
      settings = "os", "compiler", "build_type", "arch"

      def test(self):
            if self.settings.os == "Linux":
                  self.run('selftest')
