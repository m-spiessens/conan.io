from conans import ConanFile, CMake
from conans.tools import get

class mbedTLS(ConanFile):
	name="mbedtls"
	version="2.26.0"
	description="""
		Mbed TLS is a C library that implements cryptographic primitives, X.509 certificate manipulation and the SSL/TLS and DTLS protocols. 
		Its small code footprint makes it suitable for embedded systems.
		"""
	url="https://github.com/ARMmbed/mbedtls"
	license="Apache License Version 2.0"
	author="Mathias Spiessens"
	build_policy="missing"
	settings = { "arch": ["armv6", "armv7", "armv7hf", "armv8_32", "x86", "x86_64"], "os": ["none", "Linux"], "build_type": ["Release", "Debug"], "compiler": ["gcc"] }
	exports_sources = "mbed_config_armv7.h", "mbed_config_armv6.h"

	def source(self):
		get("https://github.com/ARMmbed/mbedtls/archive/v2.26.0.tar.gz")

	def build(self):
		cmake = CMake(self)

		if self.settings.os == "none":
			cmake.definitions["CMAKE_TRY_COMPILE_TARGET_TYPE"] = "STATIC_LIBRARY"
			if self.settings.arch == "armv6":
				cmake.configure(
					source_folder="mbedtls-2.26.0/",
					build_folder="build/",
					defs={ 
						"ENABLE_PROGRAMS": "OFF",
						"ENABLE_TESTING": "OFF",
						"CMAKE_C_FLAGS": "-I" + self.build_folder + " -DMBEDTLS_CONFIG_FILE='<mbed_config_armv6.h>' -march=armv6-m -mthumb -ffunction-sections -fdata-sections"
					}
				)
			elif self.settings.arch == "armv7":
				cmake.configure(
					source_folder="mbedtls-2.26.0/",
					build_folder="build/",
					defs={ 
						"ENABLE_PROGRAMS": "OFF",
						"ENABLE_TESTING": "OFF",
						"CMAKE_C_FLAGS": "-I" + self.build_folder + " -DMBEDTLS_CONFIG_FILE='<mbed_config_armv7.h>' -march=armv7e-m -mthumb -ffunction-sections -fdata-sections"
					}
				)
			elif self.settings.arch == "armv7hf":
				cmake.configure(
					source_folder="mbedtls-2.26.0/",
					build_folder="build/",
					defs={ 
						"ENABLE_PROGRAMS": "OFF",
						"ENABLE_TESTING": "OFF",
						"CMAKE_C_FLAGS_INIT": "-I" + self.build_folder + " -DMBEDTLS_CONFIG_FILE='<mbed_config_armv7.h>' -march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -ffunction-sections -fdata-sections"
					}
				)
		else:
			cmake.configure(source_folder="mbedtls-2.26.0/", build_folder="build/")

		cmake.build()
		
		if self.settings.os == "Linux":
			self.run(self.build_folder + "/build/programs/test/selftest")

	def package(self):
		self.copy("*.h", "include/", "mbedtls-2.26.0/include/")
		self.copy("*.h", "include/", "./")
		self.copy("*.a", "library/", "build/library/")
		if self.settings.build_type == "Debug":
			self.copy("*.h", "source/", "build/library/")
			self.copy("*.c", "source/", "build/library/")
		self.copy("aescrypt2", "bin/", "build/programs/aes/")
		self.copy("crypt_and_hash", "bin/", "build/programs/aes/")
		self.copy("dh_client", "bin/", "build/programs/pkey/")
		self.copy("dh_genprime", "bin/", "build/programs/pkey/")
		self.copy("dh_server", "bin/", "build/programs/pkey/")
		self.copy("ecdh_curve25519", "bin/", "build/programs/pkey/")
		self.copy("ecdsa", "bin/", "build/programs/pkey/")
		self.copy("gen_key", "bin/", "build/programs/pkey/")
		self.copy("key_app", "bin/", "build/programs/pkey/")
		self.copy("key_app_writer", "bin/", "build/programs/pkey/")
		self.copy("mpi_demo", "bin/", "build/programs/pkey/")
		self.copy("pk_decrypt", "bin/", "build/programs/pkey/")
		self.copy("pk_encrypt", "bin/", "build/programs/pkey/")
		self.copy("pk_sign", "bin/", "build/programs/pkey/")
		self.copy("pk_verify", "bin/", "build/programs/pkey/")
		self.copy("rsa_decrypt", "bin/", "build/programs/pkey/")
		self.copy("rsa_encrypt", "bin/", "build/programs/pkey/")
		self.copy("rsa_genkey", "bin/", "build/programs/pkey/")
		self.copy("rsa_sign", "bin/", "build/programs/pkey/")
		self.copy("rsa_sign_pss", "bin/", "build/programs/pkey/")
		self.copy("rsa_verify", "bin/", "build/programs/pkey/")
		self.copy("rsa_verify_pss", "bin/", "build/programs/pkey/")
		self.copy("selftest", "bin/", "build/programs/test/")

	def package_info(self):
		self.cpp_info.includedirs = ["include/"]
		self.cpp_info.srcdirs = ["source/"]
		self.cpp_info.libdirs = ["library/"]
		self.cpp_info.libs = ["mbedtls", "mbedx509", "mbedcrypto"]
		self.env_info.path = [self.package_folder + "/bin/"]
		if self.settings.os == "none":
			if self.settings.arch == "armv6":
				self.cpp_info.defines = ["MBEDTLS_CONFIG_FILE=<mbed_config_armv6.h>"]
			elif self.settings.arch == "armv7":
				self.cpp_info.defines = ["MBEDTLS_CONFIG_FILE=<mbed_config_armv7.h>"]
			elif self.settings.arch == "armv7hf":
				self.cpp_info.defines = ["MBEDTLS_CONFIG_FILE=<mbed_config_armv7.h>"]
