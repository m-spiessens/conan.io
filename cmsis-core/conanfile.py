from conans import ConanFile, CMake
from conans.tools import get

class cmsis_core(ConanFile):
	name="cmsis-core"
	version="5.7.0"
	description="""
		CMSIS core headers.
		"""
	url="https://github.com/ARM-software/CMSIS_5"
	license="Apache License Version 2.0"
	author="Content: ARM ; Package: Mathias Spiessens"
	build_policy="missing"
	settings = { "arch": ["armv6", "armv7", "armv7hf", "armv8_32"], "os": ["none"], "build_type": ["Release", "Debug"], "compiler": ["gcc"] }

	def source(self):
		get("https://github.com/ARM-software/CMSIS_5/archive/5.7.0.tar.gz")

	def build(self):
		return

	def package(self):
		self.copy("*.h", "include/", "CMSIS_5-5.7.0/CMSIS/Core/Include/")

	def package_info(self):
		self.cpp_info.includedirs = ["include/"]
