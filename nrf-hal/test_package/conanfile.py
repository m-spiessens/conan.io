from conans import ConanFile, CMake

class TEST_nrfhal(ConanFile):
      settings = { "arch": ["armv7", "armv7hf", "armv8_32"], "os": ["none"], "build_type": ["Release", "Debug"], "compiler": ["gcc"] }
      generators = "cmake"

      def test(self):
            cmake = CMake(self)
            cmake.verbose = True
            cmake.definitions["DEFINE_PART"] = "$DEFINE_PART"
            cmake.definitions["CONAN_C_FLAGS_INIT"] = "-march=armv8-m.main -mthumb"
            cmake.definitions["CONAN_CXX_FLAGS_INIT"] = "-march=armv8-m.main -mthumb"
            cmake.configure()
            cmake.build()
