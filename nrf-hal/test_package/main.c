#include "nrf_gpio.h"

int main(void) 
{
    nrf_gpio_cfg_output(0);

    while(true)
    {
        nrf_gpio_pin_toggle(0);
        for(volatile int i = 0; i < 100000; i++);
    }
    
    return 0;
}
