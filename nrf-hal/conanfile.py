from conans import ConanFile, CMake
from conans.tools import get

class nrf_hal(ConanFile):
	name = "nrf-hal"
	version = "2.4.0"
	description = """
		nrf-hal is a standalone set of drivers for peripherals present in Nordic Semiconductor's SoCs.
		"""
	url = "https://github.com/NordicSemiconductor/nrfx"
	license = "BSD-3-Clause"
	author = "Content: Nordic ; Package: Mathias Spiessens"
	build_policy = "missing"
	requires = "cmsis-core/5.7.0@arm/stable"
	settings = { "arch": ["armv7", "armv7hf", "armv8_32"], "os": ["none"], "build_type": ["Release", "Debug"], "compiler": ["gcc"] }
	generators = "cmake"
	exports_sources = "config/*.h"

	def source(self):
		get("https://github.com/NordicSemiconductor/nrfx/archive/refs/tags/v2.4.0.tar.gz")

	def build(self):
		return

	def package(self):
		self.copy("*.h", "include/", "nrfx-2.4.0/hal/")
		self.copy("*.h", "include/", "nrfx-2.4.0/mdk/")
		self.copy("nrfx_common.h", "include/", "nrfx-2.4.0/drivers/")
		self.copy("nrfx.h", "include/", "config/")
		self.copy("*.h", "include/", "config/")

	def package_info(self):
		self.cpp_info.includedirs = ["include/", "include/hal/", "include/mdk/"]
