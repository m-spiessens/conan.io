from conans import ConanFile, CMake
from conans.tools import get

class psoc6pdl(ConanFile):
	name="usbdev"
	version="2.0.0"
	description="""
		The USB Device middleware provides a full-speed USB 2.0 Chapter 9 specification compliant device framework. 
		It uses the USBFS driver from PSoC 6 Peripheral Driver Library to interface with the hardware. 
		The middleware provides support for Audio, CDC, and HID classes. 
		It also enables implementing support for other classes. 
		The USB Configurator tool makes it easy to construct a USB Device descriptor.
		"""
	url="https://github.com/cypresssemiconductorco/usbdev"
	license="Apache License Version 2.0"
	author="Content: Cypress Semiconductor Corporation ; Package: Mathias Spiessens"
	build_policy="missing"
	requires="psoc6pdl/1.6.1@cypress/stable"
	settings = { "arch": ["armv6", "armv7", "armv7hf"], "os": ["none"], "build_type": ["Release", "Debug"], "compiler": ["gcc"] }
	generators = "cmake"
	exports_sources = "CMakeLists.txt"

	def source(self):
		get("https://github.com/cypresssemiconductorco/usbdev/archive/release-v2.0.0.zip")

	def build(self):
		cmake = CMake(self)

		cmake.definitions["CMAKE_TRY_COMPILE_TARGET_TYPE"] = "STATIC_LIBRARY"
		cmake.definitions["EXTRACTED_SOURCES"] = "usbdev-release-v2.0.0/"
		if self.settings.arch == "armv6":
			cmake.definitions["CMAKE_C_FLAGS_INIT"] = "-march=armv6-m -mthumb -ffunction-sections"
		elif self.settings.arch == "armv7":
			cmake.definitions["CMAKE_C_FLAGS_INIT"] = "-march=armv7e-m -mthumb -ffunction-sections"
		elif self.settings.arch == "armv7hf":
			cmake.definitions["CMAKE_C_FLAGS_INIT"] = "-march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -ffunction-sections"

		cmake.configure()

		cmake.build()

	def package(self):
		self.copy("*.h", "include/", "usbdev-release-v2.0.0/")
		if self.settings.build_type == "Debug":
			self.copy("*.c", "source/", "usbdev-release-v2.0.0/")
		self.copy("*.a", "library/", "lib/")

	def package_info(self):
		self.cpp_info.includedirs = ["include/"]
		self.cpp_info.srcdirs = ["source/"]
		self.cpp_info.libdirs = ["library/"]
		self.cpp_info.libs = ["usbdev"]
