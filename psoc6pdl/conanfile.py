from conans import ConanFile, CMake
from conans.tools import get
import os

class psoc6pdl(ConanFile):
	name="psoc6pdl"
	version="1.6.1"
	description="""
		The Cypress PDL simplifies software development for PSoC 6 family of devices. 
		The PDL integrates device header files, startup code, and peripheral drivers into a single package.
		"""
	url="https://github.com/cypresssemiconductorco/psoc6pdl"
	license="Apache License Version 2.0"
	author="Content: Cypress Semiconductor Corporation ; Package: Mathias Spiessens"
	build_policy="missing"
	requires="core/1.1.5@cypress/stable"
	settings = { "arch": ["armv6", "armv7", "armv7hf"], "os": ["none"], "build_type": ["Release", "Debug"], "compiler": ["gcc"] }
	generators = "cmake"
	exports_sources = "CMakeLists.txt"

	def source(self):
		get("https://github.com/cypresssemiconductorco/psoc6pdl/archive/release-v1.6.1.tar.gz")

	def build(self):
		cmake = CMake(self)

		cmake.definitions["CMAKE_TRY_COMPILE_TARGET_TYPE"] = "STATIC_LIBRARY"
		cmake.definitions["EXTRACTED_SOURCES"] = "psoc6pdl-release-v1.6.1/"
		if self.settings.arch == "armv6":
			cmake.definitions["CMAKE_C_FLAGS_INIT"] = "-march=armv6-m -mthumb -ffunction-sections"
		elif self.settings.arch == "armv7":
			cmake.definitions["CMAKE_C_FLAGS_INIT"] = "-march=armv7e-m -mthumb -ffunction-sections"
		elif self.settings.arch == "armv7hf":
			cmake.definitions["CMAKE_C_FLAGS_INIT"] = "-march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -ffunction-sections"

		cmake.configure()

		cmake.build()

	def package(self):
		self.copy("*", "./", "psoc6pdl-release-v1.6.1/")
		self.copy("*.a", "library/", "lib/")

	def package_info(self):
		self.cpp_info.includedirs = ["cmsis/include/", "devices/include/", "drivers/include/"]
		self.cpp_info.libdirs = ["library/"]
		self.cpp_info.libs = ["psoc6pdl"]
		self.env_info.devicesupport = os.path.join(self.package_folder, "devicesupport.xml")
		self.cpp_info.srcdirs = ["drivers/source/"]
