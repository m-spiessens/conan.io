from conans import ConanFile, CMake, tools
from conans.tools import get
import os

class ToolchainArmNoneEabi(ConanFile):
    name = "toolchain-arm-none-eabi"
    version = "10.2.1"
    license = "GNU GENERAL PUBLIC LICENSE Version 3"
    author = "Mathias Spiessens"
    url = "https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads"
    description = "The GNU Arm Embedded Toolchain is a ready-to-use, open-source suite of tools for C, C++ and assembly programming. The GNU Arm Embedded Toolchain targets the 32-bit Arm Cortex-A, Arm Cortex-M, and Arm Cortex-R processor families. The GNU Arm Embedded Toolchain includes the GNU Compiler (GCC) and is available free of charge directly from Arm for embedded software development on Windows, Linux, and Mac OS X operating systems."
    topics = ("toolchain")
    settings = { "compiler": ["gcc"] }
    build_policy = "missing"
	
    def source(self):
        get("https://developer.arm.com/-/media/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2?revision=ca0cbf9c-9de2-491c-ac48-898b5bbc0443&la=en&hash=68760A8AE66026BCF99F05AC017A6A50C6FD832A", 
            md5="8312c4c91799885f222f663fc81f9a31", filename="gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2")

    def package(self):
        self.copy("*", dst="", src="gcc-arm-none-eabi-10-2020-q4-major/")

    def package_info(self):
        bin_folder = os.path.join(self.package_folder, "bin")
        self.env_info.CC = os.path.join(bin_folder, "arm-none-eabi-gcc")
        self.env_info.CXX = os.path.join(bin_folder, "arm-none-eabi-g++")
        self.env_info.LD = os.path.join(bin_folder, "arm-none-eabi-ld")
        self.env_info.AR = os.path.join(bin_folder, "arm-none-eabi-ar")
        self.env_info.SYSROOT = self.package_folder
        self.env_info.DEFINE_COMPILER = "gcc"
        self.env_info.path.append(bin_folder)

