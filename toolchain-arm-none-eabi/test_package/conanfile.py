from conans import ConanFile, CMake

class TEST_toolchain(ConanFile):
      settings = "os", "compiler", "build_type", "arch"

      def test(self):
            self.run('arm-none-eabi-g++ --version')
