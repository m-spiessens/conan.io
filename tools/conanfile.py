from conans import ConanFile, CMake
from conans.tools import unzip
import os

class Tools(ConanFile):
	name = "tools"
	version = "2.3"
	description = """
		Configuration tools from the Modus Toolbox. 
		"""
	url = "https://www.cypress.com/products/modustoolbox-software-environment"
	license = "GNU GPL"
	author = "Mathias Spiessens"
	build_policy = "missing"
	settings = { "arch": ["armv6", "armv7", "armv7hf", "asmp"], "os": ["none"] }
	exports_sources = "tools_2.3.tar.xz"

	def source(self):
		unzip("tools_2.3.tar.xz")

	def package(self):
		self.copy("*", "tools_2.3/", "tools_2.3/")

	def package_info(self):
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/bt-configurator/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/capsense-configurator/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/cymcuelftool-1.0/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/cype-tool/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/device-configurator/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/dfuh-tool/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/driver_media/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/modus-shell/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/openocd/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/qspi-configurator/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/seglcd-configurator/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/smartio-configurator/bin"))
		self.env_info.path.append(os.path.join(self.package_folder, "tools_2.3/usbdev-configurator/bin"))
		self.env_info.openocd = os.path.join(self.package_folder, "tools_2.3/openocd")
